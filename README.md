Role Install Kaspersky Endpoint Security для Linux
====

В данной роли два типа установки KESL:
1. Автономный инсталляционный пакет.
2. Установка через пакет rpm.

**Автономный инсталляционный пакет.**

Автономный инсталляционный пакет создается при помощи KSC.

Устанавливаем скаченный скрипт sh.

Для запуска данного метода, установить переменну в True:

```kesl_autonomous_package: True```

**Установка через пакет rpm.**

Устанавливаем скаченный пакет kesl rpm.

Копируем настройки kesl.conf.

Применяем настройки kesl.conf для kesl.

Для запуска данного метода, установить переменну в False:

```kesl_autonomous_package: False```

Variables
=====

kesl_version: "12.0.0-6672"

kesl_scan_memory_limit: 2048

kesl_max_swap_memory: 512MB

kesl_distr_url_rpm: ""

kesl_manual_package_name: "kesl-{{ kesl_version }}.x86_64.rpm"

kesl_use_ksn: "no"

kesl_update_source: "SCServer"

kesl_update_execute: "yes"

kesl_group_clean: "no"

kesl_configure_selinux: "yes"

kesl_install_license: "no"
